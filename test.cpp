//
// Created by Gregpack on 05-Sep-18.
//
#define CATCH_CONFIG_MAIN

#include "catch.hpp"

#include "reader.h"
#include <sstream>


TEST_CASE("initTest", "reader") {
    Reader buffer;
    REQUIRE (buffer.getLength() == 2);
    REQUIRE (buffer.getMatches() == 2);
}

TEST_CASE("initWithParams", "reader") {
    Reader buffer(10, 5);
    REQUIRE (buffer.getLength() == 10);
    REQUIRE (buffer.getMatches() == 5);
}

TEST_CASE("setters", "reader") {
    Reader buffer;
    buffer.setLength(10);
    buffer.setMatches(5);
    REQUIRE (buffer.getLength() == 10);
    REQUIRE (buffer.getMatches() == 5);
}

TEST_CASE("argv", "reader") {
    char *argv[6];
    argv[0] = const_cast<char *>("somefilelocation");
    argv[1] = const_cast<char *>("-n");
    argv[2] = const_cast<char *>("3");
    argv[3] = const_cast<char *>("-m");
    argv[4] = const_cast<char *>("5");
    argv[5] = const_cast<char *>("file.txt");
    int argc = 6;
    int n = 0, m = 0;
    bool from;
    string file;
    get_flags(argv, argc, n, m, from, file);
    REQUIRE (n == 3);
    REQUIRE (from);
    REQUIRE (m == 5);
    REQUIRE (file == "file.txt");
}


TEST_CASE("test1", "reader") {
    std::stringstream f;
    f << "the quick brown fox jumps over the lazy dog";
    std::stringstream output;
    Reader buffer(2, 1);
    buffer.count_phrases(f);
    buffer.result(output);
    REQUIRE (output.str() ==  "the quick (1)\nthe lazy (1)\nquick brown (1)\nover the (1)\nlazy dog (1)\njumps over (1)\nfox jumps (1)\nbrown fox (1)\n");
}

TEST_CASE("test2", "reader") {
    std::stringstream output;
    Reader buffer;
    std::stringstream f;
    f << "In the town where I was born\n"
            "Lived a man who sailed to sea\n"
            "And he told us of his life\n"
            "In the land of submarines";
    buffer.setMatches(2);
    buffer.setLength(2);
    buffer.count_phrases(f);
    buffer.result(output);
    REQUIRE (output.str() == "In the (2)\n");
}

TEST_CASE("test3", "reader") {
    std::stringstream f;
    f << "the quick brown";
    std::stringstream output;
    Reader buffer;
    buffer.setLength(2);
    buffer.setMatches(1);
    buffer.count_phrases(f);
    buffer.result(output);
    REQUIRE (output.str() == "the quick (1)\nquick brown (1)\n");
}

TEST_CASE("test4", "reader") {
    std::stringstream f ("the quick");
    std::stringstream output;
    Reader buffer (10, 5);
    buffer.count_phrases(f);
    buffer.result(output);
    REQUIRE("" == output.str());
}