# Task 1

## Build

```
$ cd build
$ cmake ..
$ make
```

## Run

```
$ ./1st_oop_task
-n X: counts phrases with X length;
-m X: shows phrases that appear in text >= X times;
```

## Test

```
$ ./unittest -s
```
