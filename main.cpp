#include "reader.h"

int main(int argc, char **argv) {
    int n, m;
    bool from;
    std::string file;
    get_flags(argv, argc, n, m, from, file);
    Reader buffer(n, m);
    switch (from) {
        case false:
            buffer.count_phrases(std::cin);
            break;
        case true:
            std::ifstream f;
            f.open(file);
            buffer.count_phrases(f);
            break;
    }

    buffer.result(std::cout);
    return 0;
}