//
// Created by Gregpack on 04-Sep-18.
// 

#include "reader.h"

void get_flags(char **argv, int &argc, int &n, int &m, bool &from, std::string &file) {
    n = 2, m = 2, from = false;
    string filename;
    if (argc == 1) return;
    else {
        for (int i = 1; i < argc; i++) {
            if (strcmp(argv[1], "-") == 0) from = false;
            else {
                if (argv[i][0] == '-') {
                    switch (argv[i][1]) {
                        case 'n' :
                            n = strtol(argv[i + 1], nullptr, 10);
                            i++;
                            break;
                        case 'm':
                            m = strtol(argv[i + 1], nullptr, 10);
                            i++;
                            break;
                        default:
                            break;
                    }
                } else {
                    from = true;
                    file = argv[i];
                }
            }
        }
    }
};

Reader::Reader(int len, int mat)
        : length(len), matches(mat) {};

void Reader::setLength(int l) {
    length = l;
};

int Reader::getLength() {
    return length;
};

int Reader::getMatches() {
    return matches;
};

void Reader::setMatches(int m) {
    matches = m;
};

void Reader::count_phrases(std::istream &in) {
    std::vector<string> buffer;
    string word;
    string currStr;
    while (buffer.size() < length - 1 && in.good()) {
        in >> word;
        buffer.push_back(word);
    }
    while (in.good()) {
        in >> word;
        buffer.push_back(word);
        currStr = "";
        for (int i = 0; i < length; i++) {
            currStr += buffer[i] + " ";
        }
        currStr.pop_back();
        phrases[currStr]++;
        buffer.erase(buffer.begin());
    }
};

void Reader::result(std::ostream &out) {
    std::vector <std::pair <int, string>> copy;
    for (auto &ph : phrases) {
        copy.emplace_back(ph.second, ph.first);
    }
    std::sort (copy.begin(), copy.end());
    for (int i = copy.size() - 1; i >= 0; i--) {
        if (copy[i].first >= matches) {
            out <<  copy[i].second << " (" << copy[i].first << ")" << std::endl;
        }
    }
}


