//
// Created by Gregpack on 04-Sep-18.
//
#ifndef INC_1ST_OOP_TASK_READER_H
#define INC_1ST_OOP_TASK_READER_H

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <map>
#include <vector>
#include <algorithm>
#include <utility>

using std::string;

void get_flags(char **, int &, int &, int &, bool &, std::string &);

class Reader {
private:
    int length;
    int matches;
    std::map<string, int> phrases;
public:
    explicit Reader(int = 2, int = 2);

    void setLength(int);
    
    int getLength();
    
    int getMatches();

    void setMatches(int);

    void count_phrases(std::istream &);

    void result(std::ostream &);
};


#endif //INC_1ST_OOP_TASK_READER_H
